package model

type PayrollReportResponse struct {
	PayrollReport PayrollReport `json:"payrollReport"`
}

type PayrollReport struct {
	EmployeeReports []*EmployeeReport `json:"employeeReports"`
}

type EmployeeReport struct {
	EmployeeId int32     `json:"employeeId"`
	AmountPaid string    `json:"amountPaid"`
	PayPeriod  PayPeriod `json:"payPeriod"`
}
type PayPeriod struct {
	StartDate string `json:"startDate"`
	EndDate   string `json:"endDate"`
}

type Payload struct {
	AmountPaid float32
	EmployeeId int32
}
