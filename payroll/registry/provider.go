package registry

import (
	"github.com/jinzhu/copier"
	"github.com/wvchallenges/se-challenge-payroll/api"
	"github.com/wvchallenges/se-challenge-payroll/data"
	"github.com/wvchallenges/se-challenge-payroll/pkg/config"
	"github.com/wvchallenges/se-challenge-payroll/pkg/database"
	"gorm.io/gorm"
	"net/http"
)

func buildDBConnection(config config.Payroll) (*gorm.DB, func(), error) {
	dbConfig := &database.DBConfiguration{}
	if err := copier.Copy(dbConfig, &config.DB); err != nil {
		return nil, func() {}, err
	}
	return database.GetDatabaseConnection(*dbConfig)
}

func buildPayrollReportStore(db *gorm.DB) (data.PayrollReportStore, error) {
	if !db.Migrator().HasTable(&data.PayrollReport{}) {
		//  create the table
		if err := db.Migrator().CreateTable(&data.PayrollReport{}); err != nil {
			return nil, err
		}
	}
	// this will create the schema if not exists
	if err := db.AutoMigrate(&data.PayrollReport{}); err != nil {
		return nil, err
	}

	return data.NewPayrollReportStore(db), nil
}

func buildPayrollReportApi(repo data.PayrollReportStore) *api.PayrollReportApi {
	return api.NewPayrollReportApi(repo)
}

func buildPayrollApi(h *api.PayrollReportApi) (http.Handler, error) {
	return api.New(h)
}
