package data

import (
	"gorm.io/gorm"
)

// PayrollReportStore - an interface for implementing DB store methods
type PayrollReportStore interface {
	Exist(model *PayrollReport) bool
	ListAll() (reports []*PayrollReport, err error)
	Create(model *PayrollReport) error
}

type payrollReportStore struct {
	db *gorm.DB
}

func NewPayrollReportStore(db *gorm.DB) PayrollReportStore {
	return &payrollReportStore{
		db: db,
	}
}

// Create it will save the PayrollReport to DB
func (p payrollReportStore) Create(model *PayrollReport) error {
	if err := p.db.Create(model).Error; err != nil {
		return err
	}
	return nil
}

// Exist - it will chek if a given PayrollReport exists in the DB
func (p payrollReportStore) Exist(model *PayrollReport) bool {
	var count int64
	var reports []*PayrollReport
	db := p.db
	if model.ID != 0 {
		db = db.Where("id = ?", model.ID)
	}
	if model.ReportID != 0 {
		db = db.Where("report_id = ?", model.ReportID)
	}
	if model.EmployeeID != 0 {
		db = db.Where("employee_id = ?", model.EmployeeID)
	}
	if len(model.JobGroup) != 0 {
		db = db.Where("job_group = ?", model.JobGroup)
	}
	if !model.Date.IsZero() {
		db = db.Where("date = ?", model.Date)
	}
	db.Find(&reports).Count(&count)
	if count > 0 {
		return true
	}
	return false
}

// ListAll - this will return all PayrollReports that are in the DB
func (p payrollReportStore) ListAll() (reports []*PayrollReport, err error) {
	db := p.db
	if err = db.Order("id asc").Order("date asc").Find(&reports).Error; err != nil {
		return
	}
	return
}

// Delete - it deletes the PayrollReport matches the given model
func (p payrollReportStore) Delete(model *PayrollReport) (err error) {
	if err = p.db.Delete(model).Error; err != nil {
		return
	}
	return
}
