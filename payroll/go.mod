module github.com/wvchallenges/se-challenge-payroll

go 1.14

require (
	github.com/google/wire v0.4.0
	github.com/jinzhu/configor v1.2.1
	github.com/jinzhu/copier v0.0.0-20201025035756-632e723a6687
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/zenazn/goji v1.0.1
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.7
)
