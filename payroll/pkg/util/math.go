package util

import (
	"fmt"
)

func BetterFormat(num float32) string {
	s := fmt.Sprintf("%.2f", num)
	return s
}
