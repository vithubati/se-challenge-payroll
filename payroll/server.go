package payroll

import (
	"github.com/wvchallenges/se-challenge-payroll/pkg/config"
	"github.com/wvchallenges/se-challenge-payroll/registry"
	"github.com/wvchallenges/se-challenge-payroll/service"
)
func New(conf config.Payroll) (*service.App, func(), error) {
	app, cleanup, err := registry.NewApp(conf)
	if err != nil {
		return nil, nil, err
	}
	service.AppService = app
	return app, cleanup, nil
}
