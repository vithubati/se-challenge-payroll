package api

import (
	"encoding/csv"
	"fmt"
	"github.com/pkg/errors"
	"github.com/wvchallenges/se-challenge-payroll/data"
	"github.com/wvchallenges/se-challenge-payroll/model"
	"github.com/wvchallenges/se-challenge-payroll/pkg/util"
	"io"
	"log"
	"mime/multipart"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	EmpReportCsvFileFormat = `\w+-\w+-[1-9]\d*.csv`
	groupAWage             = 20.00
	groupBWage             = 30.00
	overTimeRate           = 1.5
)

// reverseFormatDate- it will return time.Time for the given date string as reverse format - given string is expected to have / as separator
func reverseFormatDate(date string, separator string) (time.Time, error) {
	slice := strings.Split(date, separator)
	if len(slice) != 3 {
		return time.Time{}, errors.New("Invalid date format")
	}
	fDate := fmt.Sprintf("%s-%s-%s", slice[2], slice[1], slice[0])
	pTime, err := time.Parse("2006-1-2", fDate)
	if err != nil {
		return time.Time{}, errors.New("Invalid date format")
	}
	return pTime, err
}

// formatDate- it will return time.Time for the given date string - given string is expected to have / as separator
func formatDate(date string, separator string) (time.Time, error) {
	slice := strings.Split(date, separator)
	if len(slice) != 3 {
		return time.Time{}, errors.New("Invalid date format")
	}
	_ = fmt.Sprintf("%s-%s-%s", slice[2], slice[1], slice[0])
	pTime, err := time.Parse("2006-1-2", date)
	if err != nil {
		return time.Time{}, errors.New("couldn't parse the date")
	}
	return pTime, err
}

// isFileNameValid - it check whether report name format is valid
func isFileNameValid(name string) bool {
	var re = regexp.MustCompile(EmpReportCsvFileFormat)
	st := re.FindAllString(name, -1)
	return len(st) == 1
}

// getPayrollReportId - it returns the reportId from the filename
func getPayrollReportId(name string) int64 {
	var re = regexp.MustCompile(EmpReportCsvFileFormat)
	st := re.FindAllString(name, -1)
	if len(st) == 1 {
		slice := strings.Split(st[0], "-")
		if len(slice) == 3 {
			rSlice := strings.Split(slice[2], ".")
			if len(rSlice) == 2 {
				rId, err := strconv.ParseInt(rSlice[0], 10, 64)
				if err != nil {
					log.Printf("an error occurred wulie parsing report id %v", err.Error())
					return -1
				}
				return rId
			}
		}
	}
	return -1
}

// validatePayrollReportPayload validate the request payload
func validatePayrollReportPayload(header *multipart.FileHeader) error {
	if !isFileNameValid(header.Filename) {
		return errors.New("invalid file name")
	}
	return nil
}

// buildPayrollReports will construct slice of data.PayrollReport for each rows in the CSV file
// it is expected that the headers are always in the order [date, hours worked, employee id, job group]
func buildPayrollReports(r io.Reader, reportId int64) ([]*data.PayrollReport, error) {
	var reports []*data.PayrollReport
	reader := csv.NewReader(r)
	_, err := reader.Read() // ignoring header
	if err != nil && err != io.EOF {
		return nil, errors.Wrapf(err, "Couldn't read the file header")
	}
	if err == io.EOF {
		return reports, nil
	}
	for {
		row, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, errors.Wrapf(err, "an error occurred while reading the file")
		}
		report, err := buildPayrollReport(row[3], row[2], row[1], row[0], reportId)
		if err != nil {
			return nil, err
		}
		reports = append(reports, report)
	}
	return reports, nil
}

// buildPayrollReport will parse the values accordingly and construct a data.PayrollReport with the given values
func buildPayrollReport(jGroup, empId, wrkHr, date string, reportId int64) (*data.PayrollReport, error) {
	report := data.PayrollReport{
		ReportID:  reportId,
		JobGroup:  jGroup,
		CreatedAt: time.Now(),
	}
	eId, err := strconv.Atoi(empId)
	if err != nil {
		return nil, errors.Wrapf(err, "an error occurred while parsing the data on the file")
	}
	wh, err := strconv.ParseFloat(wrkHr, 32)
	if err != nil {
		return nil, errors.Wrapf(err, "an error occurred while parsing the data on the file")
	}
	report.EmployeeID = int32(eId)
	report.HoursWorked = float32(wh)
	pTime, err := reverseFormatDate(date, "/")
	if err != nil {
		return nil, err
	}
	report.Date = pTime
	return &report, nil
}

// buildPayrollReportResponse forms the final response model.PayrollReportResponse
func buildPayrollReportResponse(reports []*data.PayrollReport) (*model.PayrollReportResponse, error) {
	rps, err := buildEmployeeReportsResponse(reports)
	if err != nil {
		return nil, err
	}
	return &model.PayrollReportResponse{PayrollReport: model.PayrollReport{EmployeeReports: rps}}, nil
}

// buildEmployeeReportsResponse forms thr []*model.EmployeeReport for response
func buildEmployeeReportsResponse(reports []*data.PayrollReport) ([]*model.EmployeeReport, error) {
	var eReports []*model.EmployeeReport
	generated, err := buildMapOfEmployeeReports(reports)
	if err != nil {
		return nil, err
	}
	keys := make([]model.PayPeriod, 0, len(generated))
	for key := range generated {
		keys = append(keys, key)
	}
	sort.Slice(keys, func(i, j int) bool {
		iTime, err := formatDate(keys[i].StartDate, "-")
		if err != nil {
			return false
		}
		jTime, err := formatDate(keys[j].StartDate, "-")
		if err != nil {
			return false
		}
		return iTime.Before(jTime)
	})
	for _, key := range keys {
		for x, y := range generated[key] {
			report := model.EmployeeReport{
				EmployeeId: x,
				AmountPaid: fmt.Sprintf("$%v", util.BetterFormat(y.AmountPaid)),
				PayPeriod:  key,
			}
			eReports = append(eReports, &report)
		}
	}
	return eReports, nil
}

// calculatePaidAmount - calculate the total amount paid based on the given group and hours worked
func calculatePaidAmount(wkGroup string, hoursWorked float32) float32 {
	switch wkGroup {
	case "A": // fixme use constent
		return groupAWage * hoursWorked
	case "B":
		return groupBWage * hoursWorked
	default:
		return 0.00
	}
}


// buildMapOfEmployeeReports - it constructs the response structure
// it forms the pay period, calculate the total amount paid biweekly
func buildMapOfEmployeeReports(reports []*data.PayrollReport) (map[model.PayPeriod]map[int32]model.Payload, error) {
	empReports := make(map[model.PayPeriod]map[int32]model.Payload, 0)
	for _, report := range reports {
		period := getPayPeriod(report.Date)
		if _, ok := empReports[period]; ok {
			if empPayload, ok := empReports[period][report.EmployeeID]; ok {
				empPayload.AmountPaid = empPayload.AmountPaid + calculatePaidAmount(report.JobGroup, report.HoursWorked)
				empReports[period][report.EmployeeID] = empPayload
			} else {
				empPayload.AmountPaid = calculatePaidAmount(report.JobGroup, report.HoursWorked)
				empPayload.EmployeeId = report.EmployeeID
				empReports[period][report.EmployeeID] = empPayload
			}
		} else {
			empReports[period] = make(map[int32]model.Payload)
			pl := model.Payload{
				AmountPaid: report.HoursWorked * 20,
				EmployeeId: report.EmployeeID,
			}
			empReports[period][report.EmployeeID] = pl
		}
	}

	return empReports, nil
}

// getPayPeriod will return the week period which the given report falls in
func getPayPeriod(date time.Time) model.PayPeriod {
	currentYear, currentMonth, currentDay := date.Date()
	currentLocation := date.Location()
	firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
	eOfFstByWek := firstOfMonth.AddDate(0, 0, 14)
	strtOfSndByWekFmtd := eOfFstByWek.AddDate(0, 0, 1).Format("2006-01-02")
	lastOfMonthFmtd := firstOfMonth.AddDate(0, 1, -1).Format("2006-01-02")
	if currentDay <= eOfFstByWek.Day() {
		// first 2 weeks
		//return fmt.Sprintf("%s|%s", firstOfMonth.Format("2006-01-02"), eOfFstByWek.Format("2006-01-02"))
		return model.PayPeriod{
			StartDate: firstOfMonth.Format("2006-01-02"),
			EndDate:   eOfFstByWek.Format("2006-01-02"),
		}
	} else {
		// rest 2 weeks
		return model.PayPeriod{
			StartDate: strtOfSndByWekFmtd,
			EndDate:   lastOfMonthFmtd,
		}
	}
}
