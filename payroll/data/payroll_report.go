package data

import "time"

type PayrollReport struct {
	ID          int64     `json:"id" gorm:"primaryKey;autoIncrement:true;unique;not null"`
	EmployeeID  int32     `json:"employeeId"`
	HoursWorked float32   `json:"hoursWorked"`
	Date        time.Time `json:"date"`
	JobGroup    string    `json:"jobGroup,omitempty"`
	ReportID    int64     `json:"reportId,omitempty"`
	CreatedAt   time.Time `json:"createdAt,omitempty"`
}
