package database

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)

// GetDatabaseConnection return (gorm.DB or error)
func GetDatabaseConnection(dbConf DBConfiguration) (db *gorm.DB, cleanup func(), err error) {
	var timezoneCommand string
	config := &gorm.Config{
	}
	switch dbConf.Dialect {
	case DbDialectMysql:
		timezoneCommand = "SET time_zone = '+00:00'"
		db, err = newMysql(dbConf, config)
	default:
		return nil, func() {}, fmt.Errorf("database dialect %s not supported", dbConf.Dialect)
	}

	if err != nil {
		return
	}
	genDB, err := db.DB()
	cleanup = func() {
		if err := genDB.Close(); err != nil {
			log.Println(err)
		}
	}

	if dbConf.Logging {
		db.Debug()
	}
	genDB.SetMaxOpenConns(int(dbConf.MaxOpenConns))
	genDB.SetMaxIdleConns(int(dbConf.MaxIdleConns))
	genDB.SetConnMaxLifetime(*dbConf.ConnMaxLifetime)

	if dbConf.Utc {
		if _, err = genDB.Exec(timezoneCommand); err != nil {
			return nil, cleanup, fmt.Errorf("error setting UTC timezone: %w", err)
		}
	}

	return
}

func newMysql(dbConf DBConfiguration, conf *gorm.Config) (db *gorm.DB, err error) {
	url, err := dbConf.ConnectionURL()
	if err != nil {
		return nil, err
	}
	db, err = gorm.Open(mysql.New(mysql.Config{
		DSN:                       url,
		DefaultStringSize:         256,
		DisableDatetimePrecision:  true,
		DontSupportRenameIndex:    true,
		DontSupportRenameColumn:   true,
		SkipInitializeWithVersion: false,
	}), conf)
	return
}
