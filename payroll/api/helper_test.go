package api

import (
	"github.com/wvchallenges/se-challenge-payroll/data"
	"github.com/wvchallenges/se-challenge-payroll/model"
	"reflect"
	"testing"
	"time"
)

func Test_formatDate(t *testing.T) {
	type args struct {
		date string
	}
	tim, err := time.Parse("2006-1-2", "2016-11-14")
	if err != nil {
		t.Errorf("an error occurred parsing time %v", err)
		return
	}
	tests := []struct {
		name    string
		args    args
		want    time.Time
		wantErr bool
	}{
		{
			name:    "invalid date",
			args:    args{date: "12/22/200"},
			want:    time.Time{},
			wantErr: true,
		},
		{
			name:    "invalid date 2",
			args:    args{date: "12-22-200"},
			want:    time.Time{},
			wantErr: true,
		},
		{
			name:    "invalid date 2",
			args:    args{date: ""},
			want:    time.Time{},
			wantErr: true,
		},
		{
			name:    "valid date",
			args:    args{date: "14/11/2016"},
			want:    tim,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := reverseFormatDate(tt.args.date, "/")
			if (err != nil) != tt.wantErr {
				t.Errorf("reverseFormatDate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("reverseFormatDate() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getEmpId(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want int64
	}{

		{
			name: "invalid file name format 1",
			args: args{
				name: "time-report-42g.csv",
			},
			want: -1,
		},
		{
			name: "invalid file name format 2",
			args: args{
				name: "time-report-f42.csv",
			},
			want: -1,
		},
		{
			name: "invalid file name format 3",
			args: args{
				name: "time-report-0.csv",
			},
			want: -1,
		},
		{
			name: "valid file name format",
			args: args{
				name: "time-report-4.csv",
			},
			want: 4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getPayrollReportId(tt.args.name); got != tt.want {
				t.Errorf("getPayrollReportId() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isFileNameValid(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "invalidFile name test",
			args: args{
				name: "time-report-42-.csv",
			},
			want: false,
		},
		{
			name: "invalidFile format test",
			args: args{
				name: "time-report-42-.xls",
			},
			want: false,
		},
		{
			name: "valid file test",
			args: args{
				name: "time-report-42.csv",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isFileNameValid(tt.args.name); got != tt.want {
				t.Errorf("isFileNameValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_buildPayrollReport(t *testing.T) {
	pTime, err := time.Parse("2006-1-2", "2016-11-14")
	if err != nil {
		t.Errorf("couldnt pass the test data time %v", err)
		return
	}
	result := &data.PayrollReport{
		EmployeeID:  1,
		HoursWorked: 7.5,
		Date:        pTime,
		JobGroup:    "A",
		ReportID:    42,
	}
	type args struct {
		jGroup   string
		empId    string
		wrkHr    string
		date     string
		reportId int64
	}
	tests := []struct {
		name    string
		args    args
		want    *data.PayrollReport
		wantErr bool
	}{
		{
			name: "buildPayrollReport test 1",
			args: args{
				jGroup:   "A",
				empId:    "1",
				wrkHr:    "7.5",
				date:     "14/11/2016",
				reportId: 42,
			},
			want:    result,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := buildPayrollReport(tt.args.jGroup, tt.args.empId, tt.args.wrkHr, tt.args.date, tt.args.reportId)
			if (err != nil) != tt.wantErr {
				t.Errorf("buildPayrollReport() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != nil {
				got.CreatedAt = time.Time{} // clearing time as its wont be equal
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("buildPayrollReport() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getPayPeriod(t *testing.T) {
	pTime, err := time.Parse("2006-1-2", "2016-11-14")
	if err != nil {
		t.Errorf("couldnt pass the test data time %v", err)
		return
	}
	result := model.PayPeriod{
		StartDate: "2016-11-01",
		EndDate:   "2016-11-15",
	}
	type args struct {
		date time.Time
	}
	tests := []struct {
		name string
		args args
		want model.PayPeriod
	}{
		{
			name: "getPayPeriod test",
			args: args{date: pTime},
			want: result,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getPayPeriod(tt.args.date); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getPayPeriod() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculatePaidAmount(t *testing.T) {
	type args struct {
		wkGroup     string
		hoursWorked float32
	}
	tests := []struct {
		name string
		args args
		want float32
	}{
		{
			name: "calculatePaidAmount for group A",
			args: args{
				wkGroup:     "A",
				hoursWorked: 7.5,
			},
			want: 150.00,
		},
		{
			name: "calculatePaidAmount for group B",
			args: args{
				wkGroup:     "B",
				hoursWorked: 7.5,
			},
			want: 225.00,
		},
		{
			name: "calculatePaidAmount for invalid group",
			args: args{
				wkGroup:     "S",
				hoursWorked: 7.5,
			},
			want: 0.00,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculatePaidAmount(tt.args.wkGroup, tt.args.hoursWorked); got != tt.want {
				t.Errorf("calculatePaidAmount() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_buildEmployeeReportsResponse(t *testing.T) {
	pTime, err := time.Parse("2006-1-2", "2016-11-14")
	if err != nil {
		t.Errorf("couldnt pass the test data time %v", err)
		return
	}
	testReport := data.PayrollReport{
		ID:          1,
		EmployeeID:  2,
		HoursWorked: 8,
		Date:        pTime,
		JobGroup:    "A",
		ReportID:    42,
	}
	testReports := []*data.PayrollReport{&testReport}
	type args struct {
		reports []*data.PayrollReport
	}
	resultReport := model.EmployeeReport{
		EmployeeId: 2,
		AmountPaid: "$160.00",
		PayPeriod: model.PayPeriod{
			StartDate: "2016-11-01",
			EndDate:   "2016-11-15",
		},
	}
	resultReports := []*model.EmployeeReport{&resultReport}
	tests := []struct {
		name    string
		args    args
		want    []*model.EmployeeReport
		wantErr bool
	}{
		{
			name: "buildEmployeeReportsResponse test",
			args: args{
				reports: testReports,
			},
			want:    resultReports,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := buildEmployeeReportsResponse(tt.args.reports)
			if (err != nil) != tt.wantErr {
				t.Errorf("buildEmployeeReportsResponse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("buildEmployeeReportsResponse() got = %v, want %v", got, tt.want)
			}
		})
	}
}
