package config

import (
	"errors"
	"github.com/jinzhu/configor"
)

var (
	Configor *configor.Configor
)

func NewConfig(file string) (*Configuration, error) {
	if len(file) == 0 {
		return nil, errors.New("file path is required")
	}
	config := &Configuration{}

	Configor = configor.New(&configor.Config{Debug: true, ErrorOnUnmatchedKeys: true})
	if err := Configor.Load(config, file); err != nil {
		return nil, err
	}
	config.Env = Configor.GetEnvironment()
	return config, nil
}
