package data

import (
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/wvchallenges/se-challenge-payroll/pkg/config"
	"github.com/wvchallenges/se-challenge-payroll/pkg/database"
	"gorm.io/gorm"
	"testing"
	"time"
)

const testConfig = "../config/dev.yaml"

func getConfig(file string) (*config.Configuration, error) {
	conf, err := config.NewConfig(file)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to create conf from file:%v", file)
	}
	return conf, nil
}
func buildDBConnection(config config.Payroll) (*gorm.DB, func(), error) {
	dbConfig := &database.DBConfiguration{}
	if err := copier.Copy(dbConfig, &config.DB); err != nil {
		return nil, func() {}, err
	}
	return database.GetDatabaseConnection(*dbConfig)
}

func Test_payrollReportStore_Create(t *testing.T) {
	pTime, err := time.Parse("2006-1-2", "2016-11-14")
	if err != nil {
		t.Errorf("couldnt pass the test data time %v", err)
		return
	}
	conf, err := getConfig(testConfig)
	if err != nil {
		t.Errorf("couldnt get the test config %v", err)
		return
	}
	db, close, err := buildDBConnection(conf.Services.Payroll)
	if err != nil {
		t.Errorf("couldnt build DBConnection %v", err)
		return
	}
	defer close()
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		model *PayrollReport
	}
	report := &PayrollReport{
		EmployeeID:  33,
		HoursWorked: 8,
		Date:        pTime,
		JobGroup:    "A",
		ReportID:    55,
		CreatedAt:   time.Now(),
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantReports []*PayrollReport
		wantErr     bool
	}{
		{
			name: "PayrollReport store test",
			fields: fields{
				db: db,
			},
			args: args{
				model: report,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := payrollReportStore{
				db: tt.fields.db,
			}
			if err := p.Create(tt.args.model); (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
			}
			if got := p.Exist(tt.args.model); got != true {
				t.Errorf("Exist() = %v, want %v", got, true)
			}
			gotReports, err := p.ListAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotReports == nil || len(gotReports) == 0 {
				t.Errorf("ListAll() gotReports = %v, want %v", gotReports, tt.wantReports)
			}
			err = p.Delete(tt.args.model)
			if (err != nil) != tt.wantErr {
				t.Errorf("ListAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
