package database

import (
	"testing"
	"time"
)

func TestDatabase(t *testing.T) {
	dur := time.Hour
	_, cleanup, err := GetDatabaseConnection(DBConfiguration{
		Dialect:         DbDialectMysql,
		Host:            "127.0.0.1",
		Port:            3306,
		Username:        "root",
		Password:        `wave123`,
		Database:        "wave",
		MaxOpenConns:    1,
		MaxIdleConns:    1,
		Charset:         "utf8",
		ConnMaxLifetime: &dur,
	})
	if err != nil {
		t.Fatalf("Database connection failed, %v!", err)
	}
	defer cleanup()
}


//root:wave123@(mysql:3306)/wave?charset = utf8&parseTime = True&loc = Local
