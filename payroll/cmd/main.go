package main

import (
	"flag"
	"fmt"
	"github.com/pkg/errors"
	payroll "github.com/wvchallenges/se-challenge-payroll"
	"github.com/wvchallenges/se-challenge-payroll/pkg/config"
	"github.com/zenazn/goji/graceful"
	"log"
	"os"
	"syscall"
)

var (
	flags    = flag.NewFlagSet("payroll", flag.ExitOnError)
	confFile = flags.String("conf", "", "path to the platform configuration file")
)

func main() {
	flags.Parse(os.Args[1:])

	conf, err := config.NewConfig(*confFile)
	if err != nil {
		log.Fatal(errors.Wrapf(err, "failed to create conf from file:%v", *confFile))
	}
	fmt.Printf("conf: %#v", conf)
	server, cleanup, err := payroll.New(conf.Services.Payroll)
	if err != nil {
		log.Fatal(err)
	}
	defer cleanup()

	graceful.AddSignal(syscall.SIGINT, syscall.SIGTERM)
	if err := graceful.ListenAndServe(fmt.Sprintf(":%s", conf.Services.Payroll.Port), server.API); err != nil {
		log.Fatal(err)
	}
	graceful.Wait()
}
