package util

import "testing"

func Test_betterFormat(t *testing.T) {
	type args struct {
		num float32
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "with decimal ",
			args: args{num: 30.54},
			want: "30.54",
		},
		{
			name: "with trailing zeros ",
			args: args{num: 30.00},
			want: "30.00",
		},
		{
			name: "with trailing zeros 2",
			args: args{num: 30.400},
			want: "30.40",
		},
		{
			name: "with trailing zeros 2",
			args: args{num: 30},
			want: "30.00",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BetterFormat(tt.args.num); got != tt.want {
				t.Errorf("betterFormat() = %v, want %v", got, tt.want)
			}
		})
	}
}
