// +build wireinject

package registry

import (
	"github.com/google/wire"
	"github.com/wvchallenges/se-challenge-payroll/pkg/config"
	"github.com/wvchallenges/se-challenge-payroll/service"
)

func NewApp(config config.Payroll) (*service.App, func(), error) {
	panic(wire.Build(
		buildDBConnection,
		buildPayrollReportStore,
		buildPayrollReportApi,
		buildPayrollApi,
		wire.Struct(new(service.App), "*"),
	))
}
