
create-dc-network:
	docker network create wavepay

wave-up:
	docker-compose up --force-recreate --build -d

wave-down:
	docker-compose down --rmi all