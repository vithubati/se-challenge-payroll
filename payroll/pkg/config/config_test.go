package config

import (
	"reflect"
	"testing"
)

const testConfig = "../../config/dev.yaml"

func TestNewConfig(t *testing.T) {
	type args struct {
		file string
	}
	tests := []struct {
		name    string
		args    args
		want    *Configuration
		wantErr bool
	}{
		{
			name:    "no file path test",
			args:    args{file: ""},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "sample config test",
			args:    args{file: testConfig},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewConfig(tt.args.file)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewConfig() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if reflect.TypeOf(got) != reflect.TypeOf(tt.want) {
				t.Errorf("NewConfig() got type of = %v, want type of %v", reflect.TypeOf(got), reflect.TypeOf(tt.want))
				return
			}
		})
	}
}
