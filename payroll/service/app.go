package service

import (
	"net/http"
)

var AppService *App

type App struct {
	API     http.Handler
}
