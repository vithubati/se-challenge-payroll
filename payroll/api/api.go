package api

import (
	"net/http"
)

func New(payReportHandler *PayrollReportApi) (http.Handler, error) {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/payrolls/reports/upload", payReportHandler.ReportUploadHandler)
	mux.HandleFunc("/api/payrolls/reports", payReportHandler.ReportListHandler)

	return mux, nil
}
