package api

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"github.com/wvchallenges/se-challenge-payroll/data"
	"io"
	"net/http"
)

const MaxUploadSize = 1024 * 1024 * 30 // 30 MB

type PayrollReportApi struct {
	reportStore data.PayrollReportStore
}

// NewPayrollReportApi returns a new NewAPayrollReportApi handler
func NewPayrollReportApi(repo data.PayrollReportStore) *PayrollReportApi {
	return &PayrollReportApi{
		reportStore: repo,
	}
}

//ReportUploadHandler is anAPI handler to handle the payroll report upload
func (api PayrollReportApi) ReportUploadHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}
	r.Body = http.MaxBytesReader(w, r.Body, MaxUploadSize+2)
	maxMemory := int64(32 << 20) // 32MB
	if err := r.ParseMultipartForm(maxMemory); err != nil {
		http.Error(w, fmt.Sprintf("The uploaded file is too large. Please choose a file that's less than %v MB in size", MaxUploadSize/(1024*1024)), http.StatusRequestEntityTooLarge)
		return
	}

	file, header, err := r.FormFile("file")
	if err != nil {
		http.Error(w, "couldn't read the file", http.StatusBadRequest)
		return
	}
	defer file.Close()
	if err := validatePayrollReportPayload(header); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := api.persistPayrollReport(file, header.Filename); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	response := map[string]interface{}{
		"message": "file successfully uploaded!",
	}
	e := json.NewEncoder(w)
	e.SetIndent("", "  ")
	_ = e.Encode(response)
}

// persistPayrollReport will take care of the activities of a payroll upload workflow.
// it will ignore the duplicate records
func (api PayrollReportApi) persistPayrollReport(r io.Reader, fileName string) error {
	reportId := getPayrollReportId(fileName)
	if reportId == -1 {
		return errors.New("invalid file name format. couldn't get the report id")
	}
	if err := api.checkForDuplicateReport(reportId); err != nil {
		return err
	}
	reports, err := buildPayrollReports(r, reportId)
	if err != nil {
		return err
	}
	for _, report := range reports {
		if api.reportStore.Exist(report) {
			// fixme should we throw an error
			continue
		}
		if err := api.reportStore.Create(report); err != nil {
			return err
		}
	}
	return nil
}

// checkForDuplicateReport will check if the given report id was already uploaded
func (api PayrollReportApi) checkForDuplicateReport(reportID int64) error {
	if api.reportStore.Exist(&data.PayrollReport{ReportID: reportID}) {
		return errors.New(fmt.Sprintf("The given report with id %v was already uploaded.", reportID))
	}
	return nil
}

//ReportListHandler - it handles /api/payrolls/reports api
func (api PayrollReportApi) ReportListHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}
	reports, err := api.reportStore.ListAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnprocessableEntity)
		return
	}
	eReports, err := buildPayrollReportResponse(reports)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnprocessableEntity)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	e := json.NewEncoder(w)
	e.SetIndent("", "  ")
	_ = e.Encode(eReports)
}
