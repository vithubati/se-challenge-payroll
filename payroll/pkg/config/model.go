package config

import "time"

type Configuration struct {
	Services Services
	Env      string
}
type Services struct {
	Payroll Payroll
}

type Payroll struct {
	APPName string `default:"payroll"`
	Port    string
	Version string
	DB      DB
}

type DB struct {
	Username        string `default:"root"`
	Password        string `required:"true"`
	Port            int    `default:"3306"`
	Dialect         int32  `default:"MySQL"`
	Host            string
	Database        string
	Charset         string
	Utc             bool
	Logging         bool
	Singularize     bool
	MaxOpenConns    uint32
	MaxIdleConns    uint32
	ConnMaxLifetime *time.Duration
}
