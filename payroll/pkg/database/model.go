package database

import (
	"fmt"
	"strconv"
	"time"
)

type DBConfiguration struct {
	Dialect         Dialect `default:"MySQL"`
	Host            string
	Port            int
	Username        string
	Password        string
	Database        string
	Charset         string
	Utc             bool
	Logging         bool
	Singularize     bool
	MaxOpenConns    uint32
	MaxIdleConns    uint32
	ConnMaxLifetime *time.Duration
}

// ConnectionURL returns a connection string for the database.
func (d *DBConfiguration) ConnectionURL() (url string, err error) {

	switch d.Dialect {
	case DbDialectMysql:
		return fmt.Sprintf(
			"%s:%s@(%s:%v)/%s?charset=%s&parseTime=True&loc=Local",
			d.Username, d.Password, d.Host, d.Port, d.Database, d.Charset,
		), nil
	default:
		return "", fmt.Errorf(" '%v' driver doesn't exist. ", d.Dialect)
	}
}

type Dialect int32

const (
	DbDialectUnspecified Dialect = 0
	DbDialectMysql       Dialect = 1
)

var DialectName = map[int32]string{
	0: "Unspecified",
	1: "MySQL",
}

var DialectValue = map[string]int32{
	"Unspecified": 0,
	"MySQL":       1,
}

func (x Dialect) String() string {
	return enumName(DialectName, int32(x))
}

func enumName(m map[int32]string, v int32) string {
	s, ok := m[v]
	if ok {
		return s
	}
	return strconv.Itoa(int(v))
}
